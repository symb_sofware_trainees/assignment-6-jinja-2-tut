from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from SYMB_APP.config import Config
from flask_migrate import Migrate
db=SQLAlchemy()
migrate=Migrate()
def create_app(config_class=Config):
    app = Flask(__name__)
    app.config.from_object(Config)
    db.init_app(app)
    migrate.init_app(app, db)
    with app.app_context():
        from SYMB_APP.main.routes import main
        from SYMB_APP.contacts.routes import contacts
        app.register_blueprint(contacts)
        app.register_blueprint(main)
        db.create_all()
    

    return app
        