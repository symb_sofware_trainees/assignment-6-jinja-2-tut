
from SYMB_APP import db



class Contact(db.Model):
    __tablename__='Contacts'
    email = db.Column(db.Text,  nullable=False)
    name = db.Column(db.Text, nullable=False)
    phone_number = db.Column(db.Numeric, nullable=False)
    id = db.Column(db.Integer, primary_key=True)

    def __repr__(self):
        return f"Contact('{self.name}', '{self.email}','{self.phone_number}')"
