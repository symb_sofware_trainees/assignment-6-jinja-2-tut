from flask import render_template, request, Blueprint
from SYMB_APP.models import Contact
main = Blueprint('main', __name__)


@main.route("/")
@main.route("/home")
def home():
    page = request.args.get('page', 1, type=int)
    contacts = Contact.query.order_by(Contact.id.asc()).paginate(page=page, per_page=5)
    
    return render_template('home.html', contacts=contacts)


@main.route("/about")
def about():
    return render_template('about.html', title='About')