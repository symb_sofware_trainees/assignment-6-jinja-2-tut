from flask import (render_template, url_for, flash,
                   redirect, request, abort, Blueprint)
from SYMB_APP import db
from SYMB_APP.models import Contact
from SYMB_APP.contacts.forms import ContactForm

contacts = Blueprint('contacts', __name__)


@contacts.route("/contact/new", methods=['GET', 'POST'])
def new_contact():
    form = ContactForm()
    if form.validate_on_submit():
        contact = Contact(name=form.name.data, phone_number=form.phone_number.data, email=form.email.data)
        db.session.add(contact)
        db.session.commit()
        flash('Your contact has been added!', 'success')
        return redirect(url_for('main.home'))
    return render_template('create_contact.html', title='New Contact',
                           form=form, legend='New Contact')


@contacts.route("/contact/<int:contact_id>/update", methods=['GET', 'POST'])
def update_contact(contact_id):
    contact = Contact.query.get_or_404(contact_id)
    form = ContactForm()
    if form.validate_on_submit():
        contact.name = form.name.data
        contact.phone_number = form.phone_number.data
        contact.email = form.email.data
        db.session.commit()
        flash('Your contact has been updated!', 'success')
        return redirect(url_for('main.home'))
    elif request.method == 'GET':
        form.name.data = contact.name
        form.email.data = contact.email
        form.phone_number.data = contact.phone_number
    return render_template('create_contact.html', title='Update Contact',
                           form=form, legend='Update Contact')


@contacts.route("/contact/<int:contact_id>/delete", methods=['GET', 'POST'])

def delete_contact(contact_id):
    contact = Contact.query.get_or_404(contact_id)
    db.session.delete(contact)
    db.session.commit()
    flash('Your contact has been deleted!', 'success')
    return redirect(url_for('main.home'))
