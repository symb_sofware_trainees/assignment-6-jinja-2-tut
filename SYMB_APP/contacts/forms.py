from flask_wtf import FlaskForm
from wtforms import StringField, SubmitField
from wtforms.validators import DataRequired, Email, Length


class ContactForm(FlaskForm):
    name = StringField('name', validators=[DataRequired(),Length(min=2,max=50)])
    phone_number = StringField('phone_number', validators=[DataRequired(),Length(10)])
    email = StringField('Email',
                        validators=[DataRequired(), Email()])
    submit = SubmitField('Save Contact')
